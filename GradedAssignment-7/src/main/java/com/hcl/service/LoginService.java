package com.hcl.service;

import com.hcl.dao.LoginDao;
import com.hcl.entity.Login;

public class LoginService {
	LoginDao user = new LoginDao();

	public String storeNewAccount(Login details) {
		if (user.storeNewAccount(details) > 0) {
			return "Sign in Successfully";
		} else {
			return "sorry unable to sign in";
		}
	}

	public String verifyPassword(long phoneNumber, String password) {
		if (user.verifyPassword(phoneNumber, password) == true) {
			return "Successfully log In";
		} else {
			return "sorryplease train again";
		}
	}

}
