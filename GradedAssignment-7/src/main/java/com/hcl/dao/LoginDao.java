package com.hcl.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.hcl.entity.Login;
import com.hcl.rconnection.DbConnection;

public class LoginDao {
	
		public int storeNewAccount(Login login) {
			try {
				Connection connect = DbConnection.getDbConnection();
				PreparedStatement psmt= connect.prepareStatement("INSERT INTO login value(?,?,?,?)");
				
				psmt.setLong(1, login.getPhoneNumber());
				psmt.setString(2, login.getMailId());
				psmt.setString(3, login.getUserName());
				psmt.setString(4, login.getPassword());
	 
				return psmt.executeUpdate();
			}catch(Exception e) {
				return 0;
			}
		}
		public boolean verifyPassword(Long phoneNumber, String password) {
			try {
				Connection connect = DbConnection.getDbConnection();
				PreparedStatement prestmt= connect.prepareStatement("SELECT password FROM login where PhoneNumber = ?");
			
				prestmt.setLong(1,phoneNumber);
				ResultSet result= prestmt.executeQuery();
				
				if(result.next()) {
					String passcode = result.getString(1);
					if (passcode.equals(passcode)) {
						return true;
						}
					}
				}catch (Exception exp) {
				
			}
			return false;
			
		}

	
	
}

