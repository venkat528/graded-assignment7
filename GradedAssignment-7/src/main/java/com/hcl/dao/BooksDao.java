package com.hcl.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hcl.entity.Books;
import com.hcl.rconnection.DbConnection;

public class BooksDao {
	
	public List<Books> getAllBooks(){
		List<Books> list = new ArrayList<Books>();
		try {
			Connection con = DbConnection.getDbConnection();
			PreparedStatement prestmt= con.prepareStatement("SELECT * from books");
			ResultSet result = prestmt.executeQuery();
			while(result.next()) {
				Books book = new Books();
				book.setId(result.getInt(1));
				book.setTitle(result.getString(2));
				book.setGenre(result.getString(3));						
				list.add(book);
			}
		}catch(Exception exp){
		System.err.println(exp);
		}
		return list;
	}


}

